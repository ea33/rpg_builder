# RPG Builder

This app was created as a part of Experis Academy's 3 month accelerated learning program. 
The application was created in order to demonstrate the knowledge of C# basics, patterns and best practices.
User can choose between 4 RPG character classes and customize each of the classes within certain preconditions.

## Installation:
- $ git clone https://gitlab.com/ea33/rpg_builder
- Run the RPG_Builder.sln


## ASCII art credits:
rogue by Thor https://www.asciiart.eu/people/occupations/vikings
archer by ejm97 https://ascii.co.uk/art/archery
mage by mrf https://www.asciiart.eu/people/occupations/wizards
warrior by Joan Stark https://www.asciiart.eu/people/occupations/knights


## Contributors:
Valtteri Elo