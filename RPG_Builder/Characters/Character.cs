﻿using RPG_Builder.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_Builder.Items.Item;

namespace RPG_Builder.Characters
{
    internal abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public AttributeList BaseAttributes { get; set; }
        public AttributeList TotalAttributes { get; set; }
        public double Damage { get; set; }
        public double DPS { get; set; } = 1.00;
        public Dictionary<ItemSlot, Item> Items { get; set; } = new Dictionary<ItemSlot, Item>();

        public virtual void IncreaseLevel()
        {
            //increase character's level by 1
            Level++;
        }
        public virtual void CalculateDamage()
        {
            //will be overwritten in subclasses
            //declared here in order to make the function available for all Character parameters inside functions
        }

        //Calculate characters strength, dexterity, intelligence and dps based on equipped items
        public void CalculateAttributes()
        {
            //the base attributes will be first counted towards total attributes
            TotalAttributes.Dexterity = BaseAttributes.Dexterity;
            TotalAttributes.Intelligence = BaseAttributes.Intelligence;
            TotalAttributes.Strength = BaseAttributes.Strength;

            //if character has armor on any slot, add the armors attributes to the total attributes
            foreach(KeyValuePair<ItemSlot, Item> entry in Items)
            {
                if(entry.Key != ItemSlot.Weapon)
                {
                    TotalAttributes.Dexterity += entry.Value.ItemAttributes.Dexterity;
                    TotalAttributes.Intelligence += entry.Value.ItemAttributes.Intelligence;
                    TotalAttributes.Strength += entry.Value.ItemAttributes.Strength;
                } else
                {
                    //if itemslot has weapon, calculate characters damage based on weapon stats
                    Weapon weapon = (Weapon)entry.Value;
                    DPS = weapon.BaseDamage * weapon.AttacksPerSecond;
                }
            }
        }
    }
}
