﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Characters
{
    internal class Mage : Character
    {
        AttributeList baseAttributes = new AttributeList()
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 8,
        };

        AttributeList totalAttributes = new AttributeList()
        {
            Strength = 0,
            Dexterity = 0,
            Intelligence = 0,
        };

        internal Mage()
        {
            Name = "Mage";
            Level = 1;
            BaseAttributes = baseAttributes;
            TotalAttributes = totalAttributes;
            Console.WriteLine($"Mage Created, level is {Level}");
        }

        //increase level based on Mage's level up rules
        public override void IncreaseLevel()
        {
            base.IncreaseLevel();
            baseAttributes.Strength += 1;
            baseAttributes.Dexterity += 1;
            baseAttributes.Intelligence += 5;
        }

        //calculate damage based on Mage's damage rules
        public override void CalculateDamage()
        {
            base.CalculateDamage();
            Damage = Math.Round(DPS *( 1d + (TotalAttributes.Intelligence / 100d)), 2);
        }
    }
}
