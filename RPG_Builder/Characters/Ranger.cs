﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Characters
{
    internal class Ranger: Character
    {
        AttributeList baseAttributes = new AttributeList()
        {
            Strength = 1,
            Dexterity = 7,
            Intelligence = 1,
        };

        AttributeList totalAttributes = new AttributeList()
        {
            Strength = 0,
            Dexterity = 0,
            Intelligence = 0,
        };

        internal Ranger()
        {
            Name = "Ranger";
            Level = 1;
            BaseAttributes = baseAttributes;
            TotalAttributes = totalAttributes;
        }
        //increase level based on Ranger's level up rules
        public override void IncreaseLevel()
        {
            base.IncreaseLevel();
            baseAttributes.Strength += 1;
            baseAttributes.Dexterity += 5;
            baseAttributes.Intelligence += 1;
        }
        //calculate damage based on Ranger's damage rules
        public override void CalculateDamage()
        {
            base.CalculateDamage();
            Damage = Math.Round(DPS * (1d + (TotalAttributes.Dexterity / 100d)), 2);
        }
    }
}
