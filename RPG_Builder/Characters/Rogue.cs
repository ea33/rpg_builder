﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Characters
{
    internal class Rogue : Character
    {
        AttributeList baseAttributes = new AttributeList()
        {
            Strength = 2,
            Dexterity = 6,
            Intelligence = 1,
        };

        AttributeList totalAttributes = new AttributeList()
        {
            Strength = 0,
            Dexterity = 0,
            Intelligence = 0,
        };

        internal Rogue()
        {
            Name = "Rogue";
            Level = 1;
            BaseAttributes = baseAttributes;
            TotalAttributes = totalAttributes;
        }
        //increase level based on Rogue's level up rules
        public override void IncreaseLevel()
        {
            base.IncreaseLevel();
            baseAttributes.Strength += 1;
            baseAttributes.Dexterity += 4;
            baseAttributes.Intelligence += 1;
        }
        //calculate damage based on Rogue's damage rules
        public override void CalculateDamage()
        {
            base.CalculateDamage();
            Damage = Math.Round(DPS * (1d + (TotalAttributes.Dexterity / 100d)), 2);
        }
    }
}
