﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Characters
{
    internal class Warrior : Character
    {
        AttributeList baseAttributes = new AttributeList()
        {
            Strength = 5,
            Dexterity = 2,
            Intelligence = 1,
        };

        AttributeList totalAttributes = new AttributeList()
        {
            Strength = 0,
            Dexterity = 0,
            Intelligence = 0,
        };

        internal Warrior()
        {
            Name = "Warrior";
            Level = 1;
            BaseAttributes = baseAttributes;
            TotalAttributes = totalAttributes;
        }
        //increase level based on Warrior's level up rules
        public override void IncreaseLevel()
        {
            base.IncreaseLevel();
            baseAttributes.Strength += 3;
            baseAttributes.Dexterity += 2;
            baseAttributes.Intelligence += 1;
        }
        //calculate damage based on Warrior's damage rules
        public override void CalculateDamage()
        {
            base.CalculateDamage();
            Damage = Math.Round(DPS * (1d + (TotalAttributes.Strength / 100d)), 2);
        }
    }
}
