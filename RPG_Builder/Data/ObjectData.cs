﻿using RPG_Builder.Characters;
using RPG_Builder.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Data
{
    internal class ObjectData
    {
        //All of the program's objects are declared here

        //Classes will reference ObjectData's memory addresses to manipulate characters' stats
        //based on weapon and armor stats

        public static List<Character> Characters { get; } = new List<Character>
        {
            new Rogue(),
            new Ranger(),
            new Mage(),
            new Warrior()
        };

        public static List<Weapon> Weapons { get; } = new List<Weapon>
        {
            new Weapon()
            {
                Name = "Sword",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 10,
                AttacksPerSecond = 1,
                Type = Weapon.WeaponType.Sword
            },
            new Weapon()
            {
                Name = "Axe",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 15,
                AttacksPerSecond = 1.5d,
                Type = Weapon.WeaponType.Axe
            },
            new Weapon()
            {
                Name = "Bow",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 20,
                AttacksPerSecond = 0.33d,
                Type = Weapon.WeaponType.Bow
            },
            new Weapon()
            {
                Name = "Dagger",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 7,
                AttacksPerSecond = 2,
                Type = Weapon.WeaponType.Dagger
            },
            new Weapon()
            {
                Name = "Hammer",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 50,
                AttacksPerSecond = 0.15d,
                Type = Weapon.WeaponType.Hammer
            },
            new Weapon()
            {
                Name = "Staff",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 40,
                AttacksPerSecond = 0.33d,
                Type = Weapon.WeaponType.Staff
            },
            new Weapon()
            {
                Name = "Wand",
                RequiredLevel = 10,
                Slot = Item.ItemSlot.Weapon,
                BaseDamage = 99,
                AttacksPerSecond = 1,
                Type = Weapon.WeaponType.Wand
            },

        };

        public static List<Armor> Armor { get; set; } = new List<Armor>()
        {
            new Armor()
            {
                Name = "Cloth Cap",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Head,
                Type = Items.Armor.ArmorType.Cloth,
                ItemAttributes = {
                    Dexterity = 3,
                    Intelligence = 25,
                    Strength = 2,
                }
            },
            new Armor()
            {
                Name = "Leather Pants",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Legs,
                Type = Items.Armor.ArmorType.Leather,
                ItemAttributes = {
                    Dexterity = 10,
                    Intelligence = 6,
                    Strength = 5,
                }
            },
            new Armor()
            {
                Name = "Chain Boots",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Legs,
                Type = Items.Armor.ArmorType.Mail,
                ItemAttributes = {
                    Dexterity = 40,
                    Intelligence = 1,
                    Strength = 2,
                }
            },
            new Armor()
            {
                Name = "Iron Chestplate",
                RequiredLevel = 2,
                Slot = Item.ItemSlot.Body,
                Type = Items.Armor.ArmorType.Plate,                
                ItemAttributes = {
                    Dexterity = 15,
                    Intelligence = 2,
                    Strength = 35,
                }
            },
        };
    }
}
