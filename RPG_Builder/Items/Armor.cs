﻿using RPG_Builder.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Items
{
    internal class Armor : Item
    {
        //declare all the available armor types
        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }

        //force the armor type to be one of the types declared above
        internal ArmorType Type { get; set; }

        //declare a dictionary that holds information on who can wear certain type of armor
        internal static Dictionary<ArmorType, string> ArmorCharacterPairs { get; } = new Dictionary<ArmorType, string>()
        {
            {ArmorType.Cloth, "Mage"},
            {ArmorType.Leather, "Ranger, Rogue"},
            {ArmorType.Mail, "Ranger, Rogue, Warrior"},
            {ArmorType.Plate, "Warrior"},
        };

        //construct Armor object with default values for it's attributes
        internal Armor()
        {
            ItemAttributes = new AttributeList()
            {
                Dexterity = 0,
                Strength = 0,
                Intelligence = 0
            };
        }
    }
}
