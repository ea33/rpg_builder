﻿using RPG_Builder.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Items
{
    internal abstract class Item
    {
        public enum ItemSlot
        {
            Head,
            Body,
            Legs,
            Weapon
        }
        public AttributeList ItemAttributes { get; set; }

        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public ItemSlot Slot { get; set; }

    }
}
