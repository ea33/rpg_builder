﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_Builder.Items.Armor;

namespace RPG_Builder.Items
{
    internal class Weapon : Item
    {
        public enum WeaponType
        {
            Axe,
            Bow,
            Hammer,
            Staff,
            Sword,
            Wand,
            Dagger
        }
        internal static Dictionary<WeaponType, string> WeaponCharacterPairs { get; } = new Dictionary<WeaponType, string>()
        {
            {WeaponType.Axe, "Warrior"},
            {WeaponType.Bow, "Ranger"},
            {WeaponType.Hammer, "Warrior"},
            {WeaponType.Staff, "Mage"},
            {WeaponType.Sword, "Rogue, Warrior"},
            {WeaponType.Wand, "Mage"},
            {WeaponType.Dagger, "Rogue"},
        };
        public double BaseDamage { get; set; }
        public double AttacksPerSecond { get; set; }
        public WeaponType Type { get; set; }
    }
}
