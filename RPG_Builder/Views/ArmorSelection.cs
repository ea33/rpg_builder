﻿using RPG_Builder.Characters;
using RPG_Builder.Data;
using RPG_Builder.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Views
{
    internal class ArmorSelection
    {
        static string render;
        static Character characterReference;
         
        internal static void DrawScreen(Character refernce)
        {
            characterReference = refernce;
            Console.Clear();
            SetRender();
            ListenKeyPress();
        }
        internal static void SetRender()
        {
            render = "Available Armor:";

            foreach (Item item in ObjectData.Armor)
            {
                render += $"\n\n{item.Name}   Required level: {item.RequiredLevel}";
            }

            Console.WriteLine(render);
        }

        static void ListenKeyPress()
        {
            Console.WriteLine("\nType the name of an armor you want to equip or return by typing exit:");
            while (true)
            {
                var input = Console.ReadLine();
                if (input != "exit")
                {
                    //try to select an armor based on input
                    EquipArmor(input);
                    break;
                } 
                else
                {   
                    //when user types exit, return to CharacterWindow
                    CharacterWindow.DrawScreen(characterReference);
                }
            }
            Console.ReadLine();
        }

        static void EquipArmor(string name)
        {
            try
            {
                Armor searchedItem = ObjectData.Armor.First(item => item.Name == name);
                CheckArmorCompatibility(searchedItem);
            }
            catch
            {
                InvalidNameError(name);
            }
        }

        static void CheckArmorCompatibility(Armor armor)
        {
            Console.Clear();
            if (Armor.ArmorCharacterPairs[armor.Type].Contains(characterReference.Name))
            {
                if (armor.RequiredLevel <= characterReference.Level)
                {
                    characterReference.Items[armor.Slot] = armor; 
                    Console.WriteLine($"'{armor.Name}' was equipped succesfully!");
                }
                else
                {
                    Console.WriteLine($"'{armor.Name}' requires level {armor.RequiredLevel}, but {characterReference.Name} is only level {characterReference.Level}");
                }
                
            } else
            {
                Console.WriteLine($"{characterReference.Name} can't equip {armor.Type} armor");
            }
            Thread.Sleep(1500);
            DrawScreen(characterReference);
        }

        static void InvalidNameError(string name)
        {
            Console.Clear();
            Console.WriteLine($"The item '{name}' does not exist!");
            Thread.Sleep(1500);
            DrawScreen(characterReference);
        }
    }
}
