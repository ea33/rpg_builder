﻿using RPG_Builder.Characters;
using RPG_Builder.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Views
{
    internal class CharacterSelection
    {
        static string Render = @"
                           ----- PICK A CHARACTER -----


                                                                    .-.     
         (/T\)  _____        /\                                   __|=|__
         (-,-) ()____)      /__\_{)              __/\__          (_/`-`\_)
         \###/   -||       |--<<)__\        . _  \\''//          //\___/\\
        /='#'===//||        \  /  (         -( )-/_||_\          <>/   \<>
 ,OOO//  |   |    ¨¨         \/   )          .'. \_()_/           \|_._|/
 O: O:O  LLLLL                   /|           |   | . \            <_I_>
 \OOO./  || ||                   \ \          |   | .  \            |||
        C_) (_D                  ~ ~         .'. ,\_____'.         /_|_\

        Rogue(1)            Ranger(2)            Mage(3)         Warrior(4)         Exit(9)

";

        internal static void DrawScreen()
        {  
            Console.Clear();
            Console.Write(Render);
            ListenKeyPress();
        }

        internal static void ListenKeyPress()
        {
            while (true)
            {
                var input = Console.ReadKey(true);
                if (input.Key == ConsoleKey.D1)
                {
                    //open rogue's character screen
                    var rogue = ObjectData.Characters[0];
                    CharacterWindow.DrawScreen(rogue);
                    break;
                } 
                else if (input.Key == ConsoleKey.D2)
                {
                    //open ranger's character screen
                    var ranger = ObjectData.Characters[1];
                    CharacterWindow.DrawScreen(ranger);
                    break;
                }
                else if (input.Key == ConsoleKey.D3)
                {
                    //open mage's character screen
                    var mage = ObjectData.Characters[2];
                    CharacterWindow.DrawScreen(mage);
                    break;
                }
                else if (input.Key == ConsoleKey.D4)
                {
                    //open warrior's character screen
                    var warrior = ObjectData.Characters[3];
                    CharacterWindow.DrawScreen(warrior);
                    break;
                }
                else if (input.Key == ConsoleKey.D9)
                {
                    //shut down the application
                    Environment.Exit(0);
                }
            }
        }
    }
}
