﻿using RPG_Builder.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Views
{
    internal class CharacterWindow
    {
        static Character characterReference;
        internal static void DrawScreen(Character character)
        {
            characterReference = character;
            character.CalculateAttributes();
            character.CalculateDamage();
            Console.Clear();
            Console.WriteLine(@$"
  Name: {character.Name}

  Level: {character.Level}

  Strength: {character.TotalAttributes.Strength}

  Dexterity: {character.TotalAttributes.Dexterity}

  Intelligence: {character.TotalAttributes.Intelligence}

  Damage: {character.Damage}



  Level Up(1)    Armor(2)    Weapons(3)    Return(9)     

     ");
            ListenKeyPress();
        }

        internal static void ListenKeyPress()
        {
            while (true)
            {
                var input = Console.ReadKey(true);
                if (input.Key == ConsoleKey.D9)
                {
                    //load back to character selection
                    CharacterSelection.DrawScreen();
                    break;
                }
                if (input.Key == ConsoleKey.D1)
                {
                    //level up
                    characterReference.IncreaseLevel();
                    characterReference.CalculateDamage();
                    DrawScreen(characterReference);
                    break;
                }
                if (input.Key == ConsoleKey.D2)
                {
                    ArmorSelection.DrawScreen(characterReference);
                    break;
                }
                if (input.Key == ConsoleKey.D3)
                {
                    WeaponSelection.DrawScreen(characterReference);
                    break;
                }
            }
        }
    }
}
