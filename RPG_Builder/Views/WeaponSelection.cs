﻿using RPG_Builder.Characters;
using RPG_Builder.Data;
using RPG_Builder.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Builder.Views
{
    internal class WeaponSelection
    {
        static string render;
        static Character characterReference;

        internal static void DrawScreen(Character refernce)
        {
            characterReference = refernce;
            Console.Clear();
            SetRender();
            ListenKeyPress();
        }
        internal static void SetRender()
        {
            render = "Available weapons:";

            foreach (Item item in ObjectData.Weapons)
            {
                render += $"\n\n{item.Name}   Required level: {item.RequiredLevel}";
            }

            Console.WriteLine(render);
        }

        static void ListenKeyPress()
        {
            Console.WriteLine("\nType the name of a weapon you want to equip or return by typing exit:");
            while (true)
            {
                var input = Console.ReadLine();
                if (input != "exit")
                {
                    //try to select a weapon based on input
                    EquipWeapon(input);
                    break;
                }
                else
                {
                    //when user types exit, return to CharacterWindow
                    CharacterWindow.DrawScreen(characterReference);
                }
            }
            Console.ReadLine();
        }
        static void EquipWeapon(string name)
        {
            try
            {
                Weapon searchedItem = ObjectData.Weapons.First(item => item.Name == name);
                CheckWeaponCompatibility(searchedItem);
            }
            catch
            {
                InvalidNameError(name);
            }
        }
        static void CheckWeaponCompatibility(Weapon weapon)
        {
            Console.Clear();
            if (Weapon.WeaponCharacterPairs[weapon.Type].Contains(characterReference.Name))
            {
                if (weapon.RequiredLevel <= characterReference.Level)
                {
                    characterReference.Items[weapon.Slot] = weapon;
                    Console.WriteLine($"'{weapon.Name}' was equipped succesfully!");
                }
                else
                {
                    Console.WriteLine($"'{weapon.Name}' requires level {weapon.RequiredLevel}, but {characterReference.Name} is only level {characterReference.Level}");
                }
            }
            else
            {
                Console.WriteLine($"{characterReference.Name} can't equip {weapon.Type} weapons");
            }
            Thread.Sleep(1500);
            DrawScreen(characterReference);
        }
        static void InvalidNameError(string name)
        {
            Console.Clear();
            Console.WriteLine($"The item '{name}' does not exist!");
            Thread.Sleep(1500);
            DrawScreen(characterReference);
        }
    }
}
